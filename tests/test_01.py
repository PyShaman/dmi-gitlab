import pytest


def test_01():
    assert 2 + 2 == 4


@pytest.mark.xfail(reason="Expected to fail")
def test_02():
    assert 2 + 3 == 6


@pytest.mark.skip(reason="This test is skipped for a reason")
def test_03():
    assert 2 + 5 == 7


def test_04():
    assert True is True


def test_05():
    assert 2 * 2 == 5
